package lt.telesoftas.president.service;

import lt.telesoftas.president.model.Country;
import lt.telesoftas.president.repository.CountryRepo;
import lt.telesoftas.president.services.CountryService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CountryServiceTest {
    @Mock
    CountryRepo countryRepo;
    @InjectMocks
    CountryService countryService;

    @Test
    public void makeSureServiceReturnsAllCountries() {
        when(countryRepo.findAll()).thenReturn(Collections.emptyList());
        List<Country> countryList = countryService.returnAllCountries();
        Assert.assertEquals(0, countryList.size());
        verify(countryRepo, times(1)).findAll();
        System.out.println(LocalDate.now());
    }
}

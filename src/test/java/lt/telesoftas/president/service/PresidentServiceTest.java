package lt.telesoftas.president.service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import lt.telesoftas.president.dto.PresidentDTO;
import lt.telesoftas.president.exception.CountryDoesNotExist;
import lt.telesoftas.president.model.Country;
import lt.telesoftas.president.model.President;
import lt.telesoftas.president.repository.CountryRepo;
import lt.telesoftas.president.repository.PresidentRepo;
import lt.telesoftas.president.services.PresidentService;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class PresidentServiceTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    @Mock
    PresidentRepo presidentRepo;
    @InjectMocks
    PresidentService presidentService;
    @Mock
    CountryRepo countryRepo;

    Gson gson = new Gson();

    @Test
    public void tryToAddPresidentSuccessfully() {
        when(countryRepo.findByName(any())).thenReturn(Optional.of(new Country()));
        presidentService.addPresident(new PresidentDTO());
        verify(presidentRepo, times(1)).save(any());
        verify(countryRepo, times(1)).findByName(any());
    }

    @Test
    public void tryToAddPresidentSuccessfullyAndSetCountry() {
        Country country = new Country();
        country.setId(1L);
        when(countryRepo.findByName("Lithuania")).thenReturn(Optional.of(country));
        PresidentDTO presidentDTO = new PresidentDTO();
        presidentDTO.setCountry("Lithuania");
        presidentDTO.setFirstName("Valdas");
        presidentDTO.setFamilyName("Adamkus");
        String answer = presidentService.addPresident(presidentDTO);
        Assert.assertEquals(presidentDTO.getFirstName() + " " + presidentDTO.getFamilyName() + " was saved successfully", answer);
        Assert.assertFalse(country.getPresidents().isEmpty());
    }

    @Test
    public void tryToAddPresidentWithCountryThatDoesNotExist() {
        expectedException.expect(CountryDoesNotExist.class);
        expectedException.expectMessage("Leituva does not exist");
        PresidentDTO presidentDTO = new PresidentDTO();
        presidentDTO.setCountry("Leituva");
        presidentService.addPresident(presidentDTO);
    }

    @Test
    public void tryToGetTopPresidentsByTimeWithIllegalCountryName() {
        expectedException.expect(CountryDoesNotExist.class);
        expectedException.expectMessage("ithuania does not exist");
        presidentService.topPresidentsByTime("ithuania");
    }

    @Test
    public void tryToGetTopPresidentsByTimeWithGoodName() {
        when(countryRepo.findByName(anyString())).thenReturn(Optional.of(new Country()));
        when(presidentRepo.findTopByCountryLimitThree(anyString())).thenReturn(new ArrayList<>());
        presidentService.topPresidentsByTime("Lithuania");
        verify(presidentRepo, times(1)).findTopByCountryLimitThree(anyString());
    }

    @Test
    public void tryToGetTopPresidentByTimeWithGoodNameAndResult() {
        President president = new President();
        president.setFirstName("Valdas");
        president.setFamilyName("Adamkus");
        when(countryRepo.findByName(anyString())).thenReturn(Optional.of(new Country()));
       // when(presidentRepo.findTopByCountryLimitThree(anyString())).thenReturn(Arrays.asList(president));
        JsonArray response = gson.fromJson(presidentService.topPresidentsByTime("Lithuania"), JsonArray.class);
        JsonObject jsonObject = gson.fromJson(gson.toJson(president), JsonObject.class);
        Assert.assertEquals(response.get(0).getAsJsonObject(), jsonObject);
    }
}

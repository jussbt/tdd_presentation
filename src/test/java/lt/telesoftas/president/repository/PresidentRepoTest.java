package lt.telesoftas.president.repository;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.temporal.ChronoUnit;
import java.util.*;

@RunWith(SpringRunner.class)
@DataJpaTest
//@AutoConfigureTestDatabase(connection = H2)
public class PresidentRepoTest {
    @Autowired
    PresidentRepo presidentRepo;


    @Test
    public void FindTopPresidentsInUsaReturnsHowLongTheyWerePresidents() {
        Gson gson = new Gson();

        Map<String, Integer> manuallyTakenResults = new HashMap<>();
        presidentRepo.findAll()
                .stream()
                .filter(president -> president.getCountry().getName().equals("Lithuania"))
                .forEach(president -> {

                            if (manuallyTakenResults.containsKey(president.getFamilyName())) {
                                manuallyTakenResults.put(president.getFamilyName(), manuallyTakenResults.get(president.getFamilyName()) + (int) ChronoUnit.DAYS.between(president.getSinceWhen(), president.getTillWhen()));
                            } else {
                                manuallyTakenResults.put(president.getFamilyName(), (int) ChronoUnit.DAYS.between(president.getSinceWhen(), president.getTillWhen()));
                            }
                        }
                );
        List<Map<String, String>> presidents = presidentRepo.findTopByCountryLimitThree("Lithuania");
        JsonArray jsonArray = new Gson().fromJson(gson.toJson(presidents), JsonArray.class);
        Assert.assertTrue(jsonArray.size() == manuallyTakenResults.size());
        for (int i = 0; i < jsonArray.size(); i++) {
            Assert.assertTrue(jsonArray.get(i).getAsJsonObject().keySet().size() == 3);
            Assert.assertTrue(jsonArray.get(i).getAsJsonObject().get("days").getAsInt()==manuallyTakenResults.get(jsonArray.get(i).getAsJsonObject().get("familyName").getAsString()));
        }
    }
}

package lt.telesoftas.president.controller;

import lt.telesoftas.president.model.Country;
import lt.telesoftas.president.services.CountryService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(SpringRunner.class)
@WebMvcTest(CountryController.class)
public class CountryControllerTest {
    @Autowired
    MockMvc mockMvc;
    @MockBean
    CountryService countryService;

    @Test
    public void getAllCountriesTest() throws Exception {
        given(countryService.returnAllCountries()).willReturn(Arrays.asList(new Country("Europe", "Lithuania")));
        mockMvc.perform(MockMvcRequestBuilders.get("/api/countries/all"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.length()").value(1))
                .andExpect(jsonPath("$[0].name").value("Lithuania"))
                .andReturn();
    }
}

package lt.telesoftas.president.controller;

import lt.telesoftas.president.services.PresidentService;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(PresidentController.class)
public class PresidentControllerTest {
    @Autowired
    MockMvc mockMvc;
    @MockBean
    PresidentService presidentService;

    @Test
    public void createPresident() throws Exception {
        given(presidentService.addPresident(any())).willReturn("success");
        mockMvc.perform(post("/api/president/create")
                .contentType(MediaType.APPLICATION_JSON).content(new JSONObject().toString()))
                .andExpect(status().isOk())
                .andExpect(content().string("success"));
    }

    @Test
    public void presidentsByTimeInLithuania() throws Exception {
        given(presidentService.topPresidentsByTime("lithuania")).willReturn("President service was called");
        mockMvc.perform(get("/api/president/top/lithuania"))
                .andExpect(status().isOk())
                .andExpect(content().string("President service was called"));
    }
}

package lt.telesoftas.president.repository;

import lt.telesoftas.president.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CountryRepo extends JpaRepository<Country,Long> {
    Optional<Country> findByName(String name);
}

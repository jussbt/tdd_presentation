package lt.telesoftas.president.repository;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import lt.telesoftas.president.model.President;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Map;

public interface PresidentRepo extends JpaRepository<President,Long> {
    @Query(value = "select new map(p.familyName as familyName,p.firstName as firstName, sum(datediff (dd,p.sinceWhen,p.tillWhen)) as days) from President p where p.country.name=?1 group by familyName ")
    List<Map<String,String>> findTopByCountryLimitThree(String country);
}

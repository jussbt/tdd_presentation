package lt.telesoftas.president.controller;

import lt.telesoftas.president.dto.PresidentDTO;
import lt.telesoftas.president.services.PresidentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/president")
@CrossOrigin(origins = "http://localhost:3000")
public class PresidentController {

    PresidentService presidentService;

    public PresidentController(PresidentService presidentService) {
        this.presidentService = presidentService;
    }

    @PostMapping(value = "/create")
    public String addNewPresident(@RequestBody PresidentDTO presidentDTO) {
        return presidentService.addPresident(presidentDTO);
    }
    @GetMapping(value = "/top/{countryName}")
    public ResponseEntity<String> topPresidentsByTime(@PathVariable String countryName){
        return new ResponseEntity<>(presidentService.topPresidentsByTime(countryName),HttpStatus.OK);
    }
}

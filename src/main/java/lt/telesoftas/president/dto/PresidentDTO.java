package lt.telesoftas.president.dto;

import java.time.LocalDate;

public class PresidentDTO {
    private String firstName;
    private String familyName;
    private LocalDate sinceWhen;
    private LocalDate tillWhen;
   private String country;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public LocalDate getSinceWhen() {
        return sinceWhen;
    }

    public void setSinceWhen(LocalDate sinceWhen) {
        this.sinceWhen = sinceWhen;
    }

    public LocalDate getTillWhen() {
        return tillWhen;
    }

    public void setTillWhen(LocalDate tillWhen) {
        this.tillWhen = tillWhen;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}

package lt.telesoftas.president.exception;

public class CountryDoesNotExist extends RuntimeException {
    private String message;

    public CountryDoesNotExist(String message) {
        super(message);
        this.message = message;
    }
}

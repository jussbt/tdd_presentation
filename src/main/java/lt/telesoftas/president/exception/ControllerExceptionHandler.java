package lt.telesoftas.president.exception;


import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;



@ControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(value = CountryDoesNotExist.class)
    public ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request){
        return handleExceptionInternal(ex,ex.getLocalizedMessage(),new HttpHeaders(),HttpStatus.NOT_FOUND,request);
    }


}

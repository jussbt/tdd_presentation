package lt.telesoftas.president.services;

import com.google.gson.Gson;
import lt.telesoftas.president.dto.PresidentDTO;
import lt.telesoftas.president.exception.CountryDoesNotExist;
import lt.telesoftas.president.model.Country;
import lt.telesoftas.president.model.President;
import lt.telesoftas.president.repository.CountryRepo;
import lt.telesoftas.president.repository.PresidentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class PresidentService {

    PresidentRepo presidentRepo;

    CountryRepo countryRepo;
    Gson gson = new Gson();

    @Autowired
    public PresidentService(PresidentRepo presidentRepo, CountryRepo countryRepo) {
        this.presidentRepo = presidentRepo;
        this.countryRepo = countryRepo;
    }

    @Transactional
    public String addPresident(PresidentDTO presidentDTO) {
        Optional<Country> country = countryRepo.findByName(presidentDTO.getCountry());
        if (country.isPresent()) {
            President president = new President();
            president.setFirstName(presidentDTO.getFirstName());
            president.setFamilyName(presidentDTO.getFamilyName());
            president.setSinceWhen(presidentDTO.getSinceWhen());
            president.setTillWhen(presidentDTO.getTillWhen());
            country.get().addPresidet(president);
            presidentRepo.save(president);
            return president.getFirstName() + " " + president.getFamilyName() + " was saved successfully";
        } else {
            throw new CountryDoesNotExist(presidentDTO.getCountry() + " does not exist");
        }
    }

    @Transactional
    public String topPresidentsByTime(String countryName) {
        Optional<Country> country = countryRepo.findByName(countryName);
        if (!country.isPresent()) {
            throw new CountryDoesNotExist(countryName + " does not exist");
        }
        return gson.toJson(presidentRepo.findTopByCountryLimitThree(countryName));
    }
}

package lt.telesoftas.president.services;

import lt.telesoftas.president.model.Country;
import lt.telesoftas.president.repository.CountryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryService {

    CountryRepo countryRepo;

    @Autowired
    public CountryService(CountryRepo countryRepo) {
        this.countryRepo = countryRepo;
    }

    public List<Country> returnAllCountries(){
        return countryRepo.findAll();
    }
}

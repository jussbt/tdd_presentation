package lt.telesoftas.president.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
public class President {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName;

    private String familyName;

    private LocalDate sinceWhen;

    private LocalDate tillWhen;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="country_id")
    @JsonIgnore
    private Country country;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public LocalDate getSinceWhen() {
        return sinceWhen;
    }

    public void setSinceWhen(LocalDate sinceWhen) {
        this.sinceWhen = sinceWhen;
    }

    public LocalDate getTillWhen() {
        return tillWhen;
    }

    public void setTillWhen(LocalDate tillWhen) {
        this.tillWhen = tillWhen;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}

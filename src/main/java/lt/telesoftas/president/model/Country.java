package lt.telesoftas.president.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Country {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String region;
    private String name;


    @OneToMany(mappedBy = "country")
    private Set<President> presidents = new HashSet<>();

    public Country() {
    }

    public Country(String region, String name) {
        this.region = region;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Set<President> getPresidents() {
        return presidents;
    }

    public void setPresidents(Set<President> presidents) {
        this.presidents = presidents;
    }
    public void addPresidet(President president){
        this.presidents.add(president);
        president.setCountry(this);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

INSERT INTO country (id, name, region) VALUES
  (1, 'Lithuania', 'Europe'),
  (2, 'USA', 'North America')
  ;

Insert Into president(id,family_name,first_name,since_when,till_when,country_id) values
(1,'Adamkus','Valdas','1998-02-26','2003-02-26',1),
(2,'Adamkus','Valdas','2004-07-12','2009-07-11',1),
(3,'Smetona','Antanas','1926-12-19','1940-06-15',1),
(4,'Grybauskaite','Dalia','2009-05-05','2018-05-05',1),
(5,'Brazauskas','Algirdas','1993-02-25','1998-02-25',1),
(6,'Barack','Obama','2009-01-20','2017-01-20',2),
;
